import os
import re
import yaml
import unittest
from pathlib import Path
from collections import namedtuple

# Snips import
import snips_nlu.nlu_engine.nlu_engine as snips_nlu_engine

# Paths
DATADIR = Path('data')

# Named tuples (structure <3)
contents = namedtuple('yaml', 'data type fname')


def entity_exits(name: str) -> bool:
    """
       Find if a entity is defined in `./data/entities/` directory.

       How?
        - ∀ file in dir
            - parse yaml
            - find if the name matches the "name" tag
    """
    for entity_file_name in os.listdir(DATADIR / 'entities'):

        if not entity_file_name.endswith('.yaml'):
            # Only use .yaml files
            continue

        # Read Content
        with open(DATADIR / 'entities' / entity_file_name, 'r') as entity_file:
            content = yaml.load_all(entity_file.read(), Loader=yaml.FullLoader)

        # Try finding what we're looking for
        for _content in content:
            if _content.get('name', '') == name:
                return True

    else:
        return False


class IntentTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        """Go through the files in DATADIR/intents and convert everything to dicts (named tuples, really)"""
        super().__init__(*args, **kwargs)

        self.contents = []

        for intent_file_name in os.listdir(DATADIR / 'intents'):
            if not intent_file_name.endswith('.yaml'):
                continue

            with open(DATADIR / 'intents' / intent_file_name, 'r') as intent_file:
                content = yaml.load_all(intent_file.read(), Loader=yaml.FullLoader)

            for _content in content:
                self.contents.append(contents(_content, _content['type'], intent_file_name))

    def test_entities_exist(self):
        """Check if the entities mentioned in all the slots, across all intent files are in the codebase somewhere"""
        entities = {}

        # Mine all entities mentioned
        for _content in self.contents:
            for _slot in _content.data['slots']:
                entities.setdefault(_slot['entity'], []).append(_content.fname)

        # Check if they exist somewhere
        for entity in entities.keys():

            if entity.startswith('snips'):
                self.assertTrue(snips_nlu_engine.is_builtin_entity(entity), msg=f'The builtin entity - "{entity}", mentioned in '
                                                                                f'{set(entities[entity])} is not a valid builtin entity.')
                continue

            self.assertTrue(entity_exits(entity), msg=f'The entity - "{entity}", mentioned in {set(entities[entity])} has not been defined.')

    def test_only_intents(self):
        """ If an entity or something is defined in the intents folder. """
        for _content in self.contents:
            self.assertEqual(_content.type, 'intent', msg=f'An entry of {_content.type} type was found in {DATADIR/"intents"/_content.fname} .')

    def test_slots_exists_in_utterances(self):
        """ Whether an intent uses a slot which isn't defined in there. """

        for _content in self.contents:
            defined_slots = []

            # Find the slots
            for _slot in _content.data['slots']:
                defined_slots.append(_slot['name'])

            # Find the mention of slots
            for i, _utterance in enumerate(_content.data['utterances']):
                used_slots = [match[1:-1] for match in re.findall(r"\[[A-Za-z0-9_]+\]", _utterance)]
                for slot in used_slots:
                    self.assertIn(slot, defined_slots, msg=f"Utterance {i}, in intent - {_content.data['name']} uses an undefined slot. ")


class EntityTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        """Go through the files in DATADIR/entities and convert everything to dicts (named tuples, really)"""
        super().__init__(*args, **kwargs)

        self.contents = []

        for entity_file_name in os.listdir(DATADIR / 'entities'):
            if not entity_file_name.endswith('.yaml'):
                continue

            with open(DATADIR / 'entities' / entity_file_name, 'r') as intent_file:
                content = yaml.load_all(intent_file.read(), Loader=yaml.FullLoader)

            for _content in content:
                self.contents.append(contents(_content, _content['type'], entity_file_name))

    def test_only_entities(self):

        for _content in self.contents:
            self.assertEqual(_content.type, 'entity', msg=f'An entry of {_content.type} type was found in {DATADIR/"entities"/_content.fname} .')


if __name__ == "__main__":
    os.chdir('..')
    itt = IntentTest()
    itt.test_entities_exist()