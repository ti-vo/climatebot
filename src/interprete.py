import sys
import glob
from config import config as config
from importlib import import_module
import json
from snips_nlu import SnipsNLUEngine

def loadNLUEngine():
    '''
    Loads the latest NLU engine from data/models/*
    :return: nlu engine
    '''
    models = glob.glob('data/models/*')
    if not models:
        print('No pretrained models found. Please run train.py first.')
        sys.exit()
    latest_model = sorted(models)[-1]
    print("Loading following model: " + latest_model)
    engine = SnipsNLUEngine.from_path(latest_model)
    return engine

def parseText(engine, text):
    top_n_intents = engine.parse(text, top_n=config.top_n_intents)
    if config.debug:
        print(json.dumps(top_n_intents, indent=4, sort_keys=True))

    return top_n_intents[0]

def handleIntent(data):
    return_value = data
    try:
        intent = data['intent']['intentName']
        handler = import_module('handler.{intent}'.format(intent=intent)).handle
        print("Loaded handler for ", intent)
    except Exception as e:
        print("Could not load handler for ", intent, e)
        handler = lambda x: x
    return handler(data)

def main():
    argument_length = len(sys.argv)
    if argument_length < 2:
        print('Text to parse is missing.')
        sys.exit()
    elif argument_length > 2:
        print('To many arguments.')
        sys.exit()
    else:
        nlu_engine = loadNLUEngine()
        data = parseText(nlu_engine, sys.argv[-1])
        pp(handleIntent(data))
        sys.exit()


if __name__ == '__main__':
    main()
