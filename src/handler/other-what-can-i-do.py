def handle(data):
    data["answer"] = "Jeder kann etwas zum Klimaschutz beitragen! Auch du kannst ein wichtiger Teil des Wandels hin " \
                     "zu einer klimabewussten Gesellschaft sein. " \
                     "<ul>" \
                     "<li>Fahre mit dem Rad und vermeide Autofahren so oft du kannst,um CO<sub>2</sub> einzusparen.</li> " \
                     " <li>Vermeide Flugreisen - Mache in Italien Urlaub, statt auf den Malediven. Warst du zum Beispiel" \
                     " schon in Ligurien?" \
                     " <li> Trenne deinen Müll, wechsle deinen Stromanbieter, vermeide es, Plastikverpackungen zu kaufen." \
                     " <li> Lebe ressourcenschonend. Das bedeutet zum Beispiel, Dinge zu reparieren, anstatt " \
                     "sie wegzuwerfen, oder gebrauchte Artikel anstatt von neuen zu kaufen." \
                     " <li> Sag beim Einkaufen öfters: 'Ich brauche keine Plastiktüte.' "\
                     "</ul>"\
                     "* Das klingt nach sehr wenig, so als ob man als Einzelperson überhaupt nichts erreichen könnte." \
                     " Aber wenn du dich bemühst, ökologischer zu handeln, wirst du damit dein Umfeld inspirieren," \
                     " dasselbe zu tun. "\
                     "* Am wichtigsten ist es, dass du mit deinen Freunden, mit Bekannten und deiner Familie über das " \
                     "Thema Klimawandel diskutierst. Je mehr Menschen sich der Folgen bewusst werden, die unser " \
                     "gegenwärtiger Lebensstil auf das Klima und damit auf die Welt, in der die zukünftigen " \
                     "Generationen" \
                     " leben werden, hat, umso wahrscheinlicher wird es, dass wir etwas verändern können."
    return data
