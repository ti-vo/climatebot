def handle(data):
    data["answer"] = "Im Vergleich zu vorindustriellen Werten hat sich die globale " \
                     "Durchschnittstemperatur bereits um etwa 1 Grad Celsius erwärmt. Das stellte der Weltklimarat (IPCC) " \
                     "in seinem Sonderbericht von 2018 fest. " \
                     "Über Land ist die Durchschnittstemperatur demnach sogar schon um 1,53°C angestiegen."

    return data
