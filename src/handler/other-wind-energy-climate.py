def handle(data):
    data["answer"] = "Windenergie gehört zu den erneuerbaren Energiequellen und stellt einen wichtigen Teil der " \
                     "Energiewende dar. Windenergieanlagen (WEA) entziehen der Luftmasse, die sie umströmt, " \
                     "Bewegungsenergie. Allerdings ist der Anteil der entzogenen Energie so klein, dass der Effekt im " \
                     "Wettergeschehen nicht feststellbar ist."
    return data
