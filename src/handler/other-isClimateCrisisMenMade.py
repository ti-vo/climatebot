def handle(data):
    data["answer"] = "Der Klimawandel ist real. Man kann weltweit einen schnellen Temperaturanstieg messen, " \
                     "welcher auf den verstärkten Treibhauseffekt durch die erhöhte CO<sub>2</sub>-Konzentration " \
                     "in der Atmosphäre zurückzuführen ist – vor allem durch das Verbrennen fossiler Energieträger. " \
                     "<p><b>Dies ist der wissenschaftliche Konsens.</b></p> Es gibt quasi keine ernst zu nehmende " \
                     "Wissenschaftlerinnen oder Wissenschaftler, " \
                     "die den Klimawandel leugnen, oder die Tatsache, dass dieser durch den Menschen verursacht wird."

    return data
