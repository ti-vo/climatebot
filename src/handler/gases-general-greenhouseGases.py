def handle(data):
    data["answer"] = "TREIBHAUSGASE" \
                     "Treibhausgase sind Spurengase, die in der Atmosphäre vorkommen und zu einer Erwärmung der Temperatur an der Erdoberfläche führen." \
                     "Die wichtigsten Treibhausgase sind:" \
                     "* Wasserdampf (H<sub>2</sub>O)" \
                     "* Kohlendioxid(CO<sub>2</sub>)" \
                     "* Methan (CH<sub>4</sub>)" \
                     "* Lachgas (N<sub>2</sub>O)" \  
                     "* Sauerstoff und Ozon (O<sub>2</sub> und O<sub>3</sub>)" \
                     "* Schwefelhexafluorid (SF<sub>6</sub>)"
    return data
