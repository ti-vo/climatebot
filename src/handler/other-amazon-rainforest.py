def handle(data):
    data["answer"] = "<h4>Das Problem im Amazonasregenwald</h4>" \
                     "Der Regenwald im Amazonas brennt. Die Feuer sind absichtlich gelegt worden: " \
                     "Der Wald wird vernichtet, um Platz zu schaffen für Sojaanbau und Viehhaltung. " \
                     "Dadurch wird der Lebensraum vieler ohnehin schon bedrohter Pflanzen- und Tierarten " \
                     "gefährdet, außerdem wird einer der wichtigsten CO<sub>2</sub>-Speicher vernichtet. " \
                     "Das könnte das Klimasystem zum Umkippen bringen - eine große Gefahr. " \
                     "" \
                     "<h4>Was kann man tun?</h4>" \
                     "<ul>"\
                      "<li>Spenden für den Erhalt des Amazonasregenwaldes</li> " \
                     " <li>Konsumiere weniger Fleisch und Milchprodukte: Über 80% des Sojas weltweit landen laut WWF in der " \
                     "Futtermittelindustrie. Soja ist ein wichtiger Bestandteil von Kraftfutter für Masttiere und Milchkühe." \
                      "</ul>"


    data["additionalInformation"] = [{"url":"https://blog.wwf.de/5-dinge-fuer-den-amazonas/"}]

    return data
