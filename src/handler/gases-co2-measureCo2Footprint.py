def handle(data):
    data["answer"] = "<h4>CO<sub>2</sub>-Fußabdruck</h4>" \
                      "Der CO<sub>2</sub>-Fußabdruck des Durchschnittsbürgers eines Landes kann berechnet werden, indem man " \
                     "alle Treibhausgasemissionen eines Landes zusammenzählt und durch die Anzahl an Einwohnern in " \
                     "diesem Land teilt. Jeder kann allerdings auch seinen oder ihren ganz persönlichen " \
                     "CO<sub>2</sub>-Fußabdruck berechnen. Dafür stehen verschiedene Online-Tools zur Verfügung."

    data["additionalInformation"] = [{"url":"https://www.wwf.de/themen-projekte/klima-energie/wwf-klimarechner/"}]

    return data
