def handle(data):
    data["answer"] = "<h4>Treibhausgaseffekt</h4>" \
                     "Manche Spurengase wie z.B. Kohlendioxid (CO<sub>2</sub>) und Methan (CH<sub>4</sub>), absorbieren Wärmestrahlung (Infrarotstrahlung), " \
                     "die von der Erde emittiert wird. Dadurch erwärmt sich die Atmosphäre, und ein Teil dieser Wärme wird wieder zur Erde zurückgestrahlt, " \
                     "wodurch sich auch die Temperatur an der Erdoberfläche erhöht. Dieser Effekt nennt sich Treibhauseffekt. " \
                     "Zunächst handelt es sich dabei um einen ganz natürlichen Prozess, durch den die Erde übheraupt erst für uns Menschen bewohnbar wird " \
                     "(gäbe es den Treibhauseffekt nicht, läge die durchschnittliche Temperatur der Erde bei -18°C). Allerdings verstärken wir Menschen den Treibhauseffekt," \
                     "indem wir weitere Treibhausgase z.B. durch das Verbrennen fossiler Energieträger in die Atmosphäre emittieren. Dies führt dazu, dass sich die " \
                     "Temperatur der Erdoberfläche immer weiter erhöht, je mehr dieser klimawirksamen Treibhausgase in die Atmosphäre gelangen."
    data["additionalInformation"] = [{"name": "Treibhauseffekt",
                                       "description": "(Kurzwellige) Sonnenstrahlung trifft auf die Erdoberfläche, woraufhin sich diese erwärmt. "
                                                      "Dadurch wird (langwellige) Wärmestrahlung abgegeben. Treibhausgase in der Atmosphäre absorbieren "
                                                      "diese Wärmestrahlung (Infrarotstrahlung), die von der Erde emittiert wurde. "
                                                      "Die Treibhausgas-Moleküle strahlen die Wärmeenergie gleichmäßig in alle Richtungen ab, "
                                                      "d.h. auch in Richtung der Erdoberfläche. Somit erhält die Erdoberfläche nochmals Energie "
                                                      "(die so genannte Gegenstrahlung) und erwärmt sich weiter.",
                                        "images": [{"name": "Schemazeichnung zum Treibhauseffekt",
                                                    "url": "https://upload.wikimedia.org/wikipedia/commons/3/3d/Der_nat%C3%BCrliche_Treibhauseffekt.png",
                                                    "description": ""},
                                                   {"name": "Detaillierte Skizze Treibhauseffekt",
                                                    "url": "https://upload.wikimedia.org/wikipedia/commons/d/d2/Sun_climate_system_alternative_%28German%29_2008.svg",
                                                    "description": ""}
                                                   ]}]

    return data
