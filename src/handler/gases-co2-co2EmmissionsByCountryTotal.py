import json
import config.snips as snips
import config.entityMatching as entityMatching
import config.config as config

def handle(data):

  countryEntity = snips.getFirstMatchingEntity(data, "country")
  if countryEntity is None:
    data["error"] = "no-entity-found-error: " + "country"
    return data

  country = entityMatching.getMatchingCountryObject(countryEntity)
  year = snips.getFirstMatchingEntity(data, "snips/date")
  co2Data = country["data"]["co2EmmissionsByCountryTotal"]
  yearGiven = True if year is not None else False;
  unknownYear = ""

  print(year == 2001)
  print(year == "2001")
  print(yearGiven)

  # request for a year without data, so we default to 2017 data
  if year != "2017" and year != "1990" and year != "2005" and yearGiven:
    co2Value = co2Data["values"]["2017 - Fossil CO2-Emissions(Mt CO2/yr)"]
    unknownYear = f'''Für das Jahr {year} sind leider für {country["nameGerman"]} bisher keine Informationen verfügbar.'''
    year = 2017
  else:
    co2Value = co2Data["values"]["2017 - Fossil CO2-Emissions(Mt CO2/yr)"]

  # if year matches input
  data["answer"] = f'''
    {unknownYear} Die CO<sub>2</sub> Emissionen von {country["nameGerman"]} im Jahr {2017 if year is None else year} lagen bei {co2Value} Millionen Tonnen CO<sub>2</sub>/Jahr. {country["nameGerman"]} verbraucht dabei {co2Data["values"]["2017 vs 1990 - Fossil CO2-Emissions (%)"]} im Vergleich von 1990 zu 2017.
  '''.strip()
  data["additionalInformation"] = {}
  data["additionalInformation"]["data"] = co2Data

  return data