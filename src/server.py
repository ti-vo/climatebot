'''
    Creates a server by fetching the latest model.
    @TODO: Add test once the testing framework is fixed.
'''
import cgi
import logging
from flask_cors import CORS
from flask import Flask, request, jsonify, abort
from logging.config import dictConfig

# Custom code
import interprete as queryInterpreter
from config import config as cnf

app = Flask(__name__)
CORS(app)

@app.route('/query', methods=['POST'])
def query():
    '''
    :return: jsonify output.
    '''
    # @TODO: Handle breaking of SNLU engine.
    content = request.json
    question = cgi.html.escape(content['question']) # Sanitizing the input.
    try:
        app.logger.info("USER-QUESTION: " + question)
        parsed_text = queryInterpreter.parseText(engine=nlu_engine, text=question)
        parsed_text = queryInterpreter.handleIntent(parsed_text)
    except Exception as e:
        # @TODO: Handle different excepts appropriately.
        # @TODO: Test this exception.
        app.logger.error(e)
        abort(500)
    return jsonify(parsed_text)

if __name__ == '__main__':
    # Setting up logging
    dictConfig(cnf.logging_config)
    log_handler = logging.getLogger()

    # Server file configuration.
    url   = cnf.server_config['url']
    port  = cnf.server_config['port']
    debug = cnf.server_config['debug']
    app.logger.debug(f'Server is running on port {port}')
    app.logger.debug(f'Server is running on url {url}')

    # Load snips engine.
    nlu_engine = queryInterpreter.loadNLUEngine()

    #@TODO: Run test Module

    # Starting the app
    try:
        app.run(host=url, port=port, debug=debug)
    except OSError as e:
        if e.errno == 98:
            print("address already in use, try different port number")
            app.logger.error(e)
    except Exception as e:
        app.logger.error(e)
