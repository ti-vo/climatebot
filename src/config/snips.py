import json

"""
Searches for the first matching entity in all slots
found by SNIPS.

:param data: the snips data object
:param snipsType: the entity type to search for
:return: returns the raw value of the first entity
  matching the given entity type, None if no entity
  was found or the input was faulty
"""
def getFirstMatchingEntity(data, snipsType):
  # data object is invalid
  if data is None or "slots" not in data or len(data["slots"]) == 0:
    return None
  else:
    # check each slot for suitable type
    for slot in data["slots"]:
      if slot["entity"] == snipsType:
        return slot["rawValue"]

    # no suitable type found
    return None

"""
Pretty prints snips data object (JSON) to standard out.

:param data: the snips data object
"""
def prettyPrintSnipsData(data):
  print(json.dumps(data, indent=4, sort_keys=True))