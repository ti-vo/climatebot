import json

debug             = False
language          = 'de'
summarySentences  = 2
wikipediaLanguage = language
top_n_intents     = 3

'''
    @priyansh:
    This import doesn't work when configs are imported from some directories (TEST).
    Adding some redundancy here to make things work.

    I don't think this is the **best** way of doing this. Please see if you can improve this snippet here
'''
try:
    with open('data/countries.json') as countriesFile:
        countries = json.load(countriesFile)
except FileNotFoundError:
    with open('../data/countries.json') as countriesFile:
        countries = json.load(countriesFile)

# Server file configuration.
server_config = dict(
    port  = '9000',
    url   = '0.0.0.0',
    debug = True
)

# Logging file configuration.
logging_config = dict(
    version = 1,
    formatters = {
        'f': {'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
    },
    root = {
        'level': 'DEBUG',
    }
)