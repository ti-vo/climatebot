var fs = require("fs");
var oldCountries = JSON.parse(fs.readFileSync("countries.json"));
var newCountries = JSON.parse(fs.readFileSync("newCountries.json"));

Object.keys(oldCountries).forEach(function(oldCountryName){
    newCountries.forEach(function(newCountry){
        if ( newCountry.englishName == oldCountryName) {

            newCountry.data = oldCountries[oldCountryName]
        }
    });
});

console.log(JSON.stringify(newCountries, null, 4));